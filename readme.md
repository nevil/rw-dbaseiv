## 一个读取、生成DBF的软件包

### 这个软件包作废了，转移到[dbf-js](https://www.npmjs.com/package/dbf-js)。
因为这个包只能读取dbaseiv格式的文件，而不能成功解析foxpro。而在[dbf-js](https://www.npmjs.com/package/dbf-js)
中，进行了相关优化。

---------------

去[dbf-js](https://www.npmjs.com/package/dbf-js)看看
