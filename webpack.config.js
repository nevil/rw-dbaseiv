"use strict";

const path = require('path');

module.exports = {

    mode: 'production',
    entry: './src/index.js',
    externals: ['iconv-lite', 'path', 'printf', 'underscore', 'fs'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].js",
        library: "rw-dbaseiv",
        libraryTarget: "umd",
        globalObject: 'this'
    }

};