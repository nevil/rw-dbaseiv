"use strict";

const fs = require('fs');
const _ = require('underscore');
const iconv = require('iconv-lite');

module.exports = function (file, encoding="gb18030") {
    let header = {}, data = [];
    //PARSE HEADER
    let buffer = fs.readFileSync(file);
    header.type = buffer.slice(0, 1).toString(); //file type
    header.dateUpdated = parseDate(buffer.slice(1,4));
    header.recordNumbers = convertBinaryToInteger(buffer.slice(4,8));
    header.headerByteCost = convertBinaryToInteger(buffer.slice(8,10));
    header.fieldNumber = (header.headerByteCost - 32 -1) / 32;
    header.recordByteCost = convertBinaryToInteger(buffer.slice(10,12));
    let fields = [];
    for(let i=0; i<header.fieldNumber; i++) {
        fields.push(buffer.slice(32 + i * 32, 32 + i * 32 + 32));
    }
    header.fields = _.map(fields, (f)=>parseTitle(f, encoding));

    //DATA
    let sequenceNumber = 0;
    for(let i=0; i<header.recordNumbers; i++) {
        let rowBuf = buffer.slice(header.headerByteCost+i*header.recordByteCost, header.headerByteCost+(i+1)*header.recordByteCost);
        data.push(parseRecord(++sequenceNumber, rowBuf, header, encoding));
    }
    return {header, data};
};

const parseDate = function(buffer) {
    const year = 2000 + convertBinaryToInteger(buffer.slice(0,1));
    const month = convertBinaryToInteger(buffer.slice(1,2)) - 1;
    const date = convertBinaryToInteger(buffer.slice(2,3));
    return new Date(year, month, date);
};

const convertBinaryToInteger = function(buffer) {
    return buffer.readUIntLE(0, buffer.length);
};

const parseTitle = function(buffer, encoding) {
    return {
        name: iconv.decode((buffer.slice(0, 11)).toString(), encoding).replace(/[\u0000]+$/, ''),
        type: buffer.slice(11,12).toString(),
        size: convertBinaryToInteger(buffer.slice(16,17)),
        precision: convertBinaryToInteger(buffer.slice(17,18))
    };
};

const parseRecord = function (sequenceNumber, buffer, header, encoding) {
    let record = {
        '@sequenceNumber': sequenceNumber,
        '@deleted': [42, '*'].includes((buffer.slice(0,1))[0])
    };
    buffer = buffer.slice(1, buffer.length);
    for(let i=0; i<header.fieldNumber; i++) {
        let field = header.fields[i];
        record[field.name] = parseField(field, buffer.slice(0, field.size), encoding);
        buffer = buffer.slice(field.size, buffer.length);
    }
    return record;
};

const parseField = function (field, buffer, encoding) {
    let value = iconv.decode(buffer, encoding).trim();
    if(field.type === 'C') {
        value = value;
    } else if(field.type === 'L') {
        if(_.contains(['Y', 'y', 'T', 't'], value)) {
            value = true;
        } else if(_.contains(['N', 'n', 'F', 'f'], value)) {
            value =false;
        } else {
            value = null;
        }
    } else if(field.type === 'N') {
        value = parseFloat(value);
    } else if(field.type === 'D') {
        let x = new Date();
        x.setFullYear(parseInt(value.substr(0,4)));
        x.setMonth(parseInt(value.substr(4,2))-1);
        x.setDate(parseInt(value.substr(6, 2)));
        value = x;
    }
    return value;
};