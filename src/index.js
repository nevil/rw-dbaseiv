"use strict";

const readFun = require('./reader');
const writeFun = require('./writer');

module.exports = {
    read: readFun,
    write: writeFun
};