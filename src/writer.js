"use strict";

const _ = require('underscore');
const iconv = require('iconv-lite');
const printf = require('printf');

const bufferPadding = function (src, length, rightPadding = true) {
    let dst = new Uint8Array(new ArrayBuffer(length));
    let valuedSize = src.length > length ? length : src.length;
    let filledSize = length - valuedSize;
    if(rightPadding) {
        for(let i=0; i<valuedSize; i++) dst[i]=src[i];
        for(let i=0; i<filledSize; i++) dst[valuedSize+i]=0x20;
    } else {
        for(let i=0; i<filledSize; i++) dst[i]=0x20;
        for(let i=0; i<valuedSize; i++) dst[filledSize+i]=src[i];
    }
    return dst;
};

const writeField = function(view, offset, string, encoding) {
    let buf = iconv.encode(string, encoding);
    _.forEach(buf, (byte, index)=>{ if(index>=11) return; view.setUint8(offset+index, byte); });
    return;
};

const writeString = function(view, offset, string, encoding, length) {
    let strBuf = iconv.encode(string, encoding);
    let buf = bufferPadding(strBuf, length, true);
    _.forEach(buf, (byte, index)=>{ view.setUint8(offset+index, byte); });
    return offset+length;
};

const writeNumber = function(view, offset, value, precision, length) {
    value = printf(`%.${precision?precision:0}f`, value);
    let valBuf = iconv.encode(value, "ASCII");
    let buf = bufferPadding(valBuf, length, false);
    _.forEach(buf, (byte, index)=>{ view.setUint8(offset+index, byte); });
    return offset+length;
};

const writeDate = function(view, offset, value) {
    let date = '19000001';
    if(_.isDate(value)) {
        date = `${value.getFullYear()*10000+value.getMonth()*100+100+value.getDate()}`;
    } else if(_.isString(value)){
        date = value.substr(0, 8);
    }
    return writeString(view, offset, date, 'ASCII', 8);
};

const computeBytePerRec = function(fields) {
    return _.reduce(fields, (sum, f)=>{ return sum+f.size; }, 1);
};

module.exports = function (fields, data, encoding="gb18030") {
    let now = new Date(), offset=0;
    _.forEach(fields, (field)=>{
        if(!field.size || field.size===0) {//fix field size
            if(field.type === 'C') field.size = 10;
            else if(field.type === 'N') { field.size = 10; field.precision = 0; }
            else if(field.type === 'L') { field.size = 1; }
            else if(field.type === 'D') { field.size = 8; }
            else { throw new Error(`Unknown field type: ${field.type}`); }
        }
    });
    let fieldDescLength = (32 * fields.length) + 1;
    let bytePerRecord = computeBytePerRec(fields);
    let buffer = new ArrayBuffer(32 + fieldDescLength + (bytePerRecord * data.length) +1 );
    let view = new DataView(buffer);

    //FILL HEADER
    view.setUint8(0, 0x03); //version
    view.setUint8(1, now.getFullYear() - 2000); //year
    view.setUint8(2, now.getMonth() + 1); //month
    view.setUint8(3, now.getDate());  //date
    view.setUint32(4, data.length, true); //record number
    view.setUint16(8, fieldDescLength + 32, true); //header cost
    view.setUint16(10, bytePerRecord, true); //byte cost per record
    //byte 12-31 reserved, fill with 0
    _.forEach(fields, (f, i)=>{ //field description
        writeField(view, 32 + i * 32, f.name, encoding); //field name, max 11 chars
        view.setInt8(32 + i * 32 + 11, f.type.charCodeAt(0)); //field type
        //byte 12-15 reserved
        view.setInt8(32 + i * 32 + 16, f.size);
        if(f.type === 'N') view.setInt8(32 + i * 32 + 17, (f.precision ? f.precision : 0));
        //byte 18-31 reserved
    });
    view.setUint8(32 + fieldDescLength - 1, 0x0D ); //last 2 bytes 0x0D20
    view.setUint8(32 + fieldDescLength , 0x20 );
    //FILL DATA
    offset = fieldDescLength + 32;
    _.forEach(data, (row, i)=>{
        view.setUint8(offset, 0x20); offset++; //flag 0x20
        _.forEach(fields, (f) => {
            let val = row[f.name];
            if(val === null || typeof val === 'undefined') {
                if(f.type==='C') val = '';
                if(f.type==='N') val = 0;
                if(f.type==='D') val = now;
                if(f.type==='L') val = false;

            }
            switch (f.type) {
                case 'L':
                    view.setUint8(offset, val===true ? 0x54 : 0x46);
                    offset++;
                    break;
                case 'D':
                    offset = writeDate(view, offset, val);
                    break;
                case 'N':
                    offset = writeNumber(view, offset, val, f.precision, f.size);
                    break;
                case 'C':
                    offset = writeString(view, offset, val, encoding, f.size);
                    break;
                default:
                    throw new Error(`Unknown field type: ${f.type}`);
            }
        });
    });
    view.setUint8(offset, 0x1A); //end flag
    return Buffer.from(view.buffer);
};