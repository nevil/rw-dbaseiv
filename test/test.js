"use strict";

const fs = require('fs');
const write = require('../dist/main').write;
const read  = require('../dist/main').read;

let header = [
    {name: 'A', size:5, type:'C'},
    {name: 'B', size:3, precision:1, type:'N'},
    {name: 'C', type:'D'},
    {name: 'D', type:'L'},
    {name: 'E', type:'N', size:3}
];

let body = [
    {
        A: '张三三',
        B: 5.5,
        C: new Date(),
        D: true,
        E: 30
    },
    {
        A: '王',
        B: 6.0,
        C: new Date(),
        D: true,
        E: 26.2
    }
];

let buf = write(header, body, 'gb18030');

fs.writeFileSync('./test.dbf', buf);

let {header: Aheader, data:Adata} = read('./test.dbf', 'gb18030');

console.log(Aheader);
console.log(Adata);
